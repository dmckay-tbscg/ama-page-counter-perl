##                          PageCounter program.
##-------------------------------------------------------------------------------------
##
###  YOU MAY COMMENT OUT INFORMATION BY USING A # AS THE FIRST CHARACTER IN THE LINE. 
##
##-------------------------------------------------------------------------------------
##             TBSCG - The Banyan Solutions Consulting Group - Tampa FL
##                          PageCounter.pl   
##-------------------------------------------------------------------------------------
## 
##             Program :    PageCounter
##             Author:      Dan McKay
##             Date:        May, 2015
## 
##-------------------------------------------------------------------------------------
##-------------------------------------------------------------------------------------
#
#!/usr/bin/perl 
  # use strict;
   use File::Copy;
   use File::Find;
   use File::stat;
   use Config::IniFiles;
   
   use Cwd;
   ### Get File title info via URLs
   ###use LWP::Simple; ## this is used for Extract-Title having only the URL
   ##  Execle
   use Excel::Writer::XLSX;
   ##  Sending Email 
   use Mail::Sendmail qw(sendmail %mailcfg);
   use MIME::Lite;
   $mailcfg{smtp} = [qw(localhost)];
   $mailcfg{retries} = 2;
   $mailcfg{delay} = 5;   

#########  constants declaration area   ####################
   use constant false => 0;
   use constant true  => 1;
   
#########  Command line Switches   ####################
   my  $displayExceptionLog = $ARGV[0];
   my  $displayLog          = $ARGV[1];

############  Program  Information   #########################
   my $programName = $0;
   my $programPath = cwd;
      chomp($programPath);
      $programPath.="/";
      $programPath =~ s/\\/\//g;  ## for Unix  /
      ###$programPath =~ s/\//\\/g;  for windows \      
 
####### ------------------------------------------------------
# subroutines
#######  -----------------------------------------------------
   
   sub Archive;
   sub archiveLogInfo;
   sub AddArchiveFileHTML;
   
   sub OpenLogFile;
   sub LogIt; 
   sub OpenExceptionFile;
   sub logException;
   sub HandleErrors;
   
   sub printExceptionLogToConsole;
   sub printLogToConsole;
   
   sub getINIFileParameters;
   sub printIniFileContents;
   sub PadIt;

   sub OpenInputFileRead;

   sub CopyFile;
   sub MoveFile;

   sub InitializeArrays;
   
   sub RetrieveAMAData;
   sub RetrieveResourceTxt;
   sub ProcessAMAHTML;
   sub SwapHTML;
   sub AddLogFileHTML;
   sub OpenHtmlLogFileOut;
   
   sub OpenHtmlMenuIn;
   sub OpenHtmlin;
   sub OpenHtmlOut;
   sub OpenLogFileHtmlIn;
   
   sub OpenHTMLMenuOut;
   sub WriteHTMLContents;
   sub WriteHTMLPage;

   sub OpenExcelFile;
   sub LoadXLSXFile;
   
   sub SendEmailOut;
 
############   Misc Date Variables    ####################
   my $mySeconds          = (localtime)[0];#  Seconds
   #  print "Second       = $mySeconds\n" ;
   PadIt($mySeconds,1);
   #print "Second          = $mySeconds\n" ;
   my  $myMinutes         = (localtime)[1];# Minutes
   #print "Minute          = $myMinutes\n" ;
   PadIt($myMinutes,1);
   my $myHours            = (localtime) [2];# Hours
   #print "Hour            = $myHours\n" ;
   PadIt($myHours,1);
   my $myDays             = (localtime)[3];# Day
   #print "Day             = $myDays\n" ;
   PadIt($myDays,1);
   #print "Day             = $myDays\n" ;  
   my $myMonth            = (localtime)[4] + 1;# Month
   #print "Month           = $myMonth\n" ;
   PadIt($myMonth,1); 
   #print "Month           = $myMonth\n" ;
   my $myYear             = (localtime)[5] + 1900;## Year
   #print "Year            = $myYear\n" ;
   my $myCentury          = substr($myYear,0,2);
   #print "myCentury       = $myCentury\n" ;
   my $my2DigitYear       = substr($myYear,2,2);
   #print "my2DigitYear    = $my2DigitYear\n" ;

############   Misc Directory and File name Variables    ####################
##  Local Log File path and name
   my $FileDatePrefix     = "${myMonth}-${myDays}-${myYear}_";
   my $LogFileDirectory   = $programPath."Output/Logfiles/" ;
   my $LogFileName        = $FileDatePrefix."Log.txt";

##  Local Exception File path and name
   my $LogFileArchiveDirectory;
   my $ExceptionFileName  = $FileDatePrefix."Exception.txt";
##  Local INI input parameter File path and name

###     my $LocalINIFile      = $programPath."Output/PageCounter.ini";   
   my $LocalINIFile      = $programPath."PageCounterConfig/PageCounter.ini";
   my $development       =  true;
   my $xlOutputDirectory;
   my $archiveDirectory;
   my $outDirectoryContent;
   my $outDirectoryOnly;
   my $outDirectoryContentDir;
   my $outDirectoryOnlyDir;
   
   my $outHTMLMenu;
   my $outHTMLMenuDir;
   my $htmlAMAReportName;
   my $htmlResourceReportName;
   my $htmlReportNameDir;
   my $htmlOriginalReportName;
   my $htmlTemplateSelectorName;
   my $htmlSelectorName;
   my $htmlTemplateArchivePageSelector;
   my $htmlArchivePageSelector;
   
   # Directory
   ## this value is from the ini file and tells where to start pulling info from...usually the root directory
   my $amaFile;
   my $amaFileDir;
   my $resourceFile;
   my $resourceFileDir;   

############  xlsx output Variables   #########################
   my $workbook;
   my $workSheet1;
   my $workSheet2;
   my $workSheet3;
   my $xlFileName;
   my $xlExtension;
   my $worksheet_1;
   my $wkSht1Col1;
   my $wkSht1Col2;
   my $wkSht1Col3;
   my $wkSht1Col4;  
   my $worksheet_2;
   my $wkSht2Col1;
   my $wkSht2Col2;
   my $wkSht2Col3;
   my $wkSht2Col4;   
   my $worksheet_3;
   my $wkSht3Col1;
   my $wkSht3Col2;
   my $wkSht3Col3;
   
   my $ReportDelimiter ;

#########  error variable declaration area   ####################
   my $ErrorCode          = 0;
   my $ErrorLocation      = ""; 
   my $ErrorReason        = ""; 
   my $EndProgram         = false;
   my $ThereWasAnError    = false;
   my $FromError          = false;

#########  misc variable declaration area   ####################
   
   my $recipient;
   my $errorRecipient;
   my $fromSender;
   my $errorEmail;
   my $emailMessage;
   my $emailSubject;
   my $errorEmailMessage;
   my $errorEmailSubject;

   my $ProcessedFiles = false;

   ##  also from the ini file; flag used to select to use link in output html page or not
   ##    initialized to use STATiC display
   my $htmlUseLink = false;
   ## used to hold number of archive days for html and log output files
   my $HTMLArchiveDays;
   my $LogArchiveDays;
   
   my $lastDirName   = "";
   my @lastDirArray  = ();
   my $lastDirCnt    = 0;
   my @curDirArray   = ();
   my $curDirCnt     = 0;
   my $diffCnt       = 0;

####### ------------------------------------------------------
# array, hash Variables
#######  
   my @amaInArray         = ();
   my @directoryNameArray = ();
   my @sortedDirNameArray = ();
   my @finalAMAArray      = ();
      $arrayRowCnt        = 0;
   my @xlsWkSht1          = ();
   my $wkSht1RowCnt       = 0;
   my @xlsWkSht2          = ();
   my $wkSht2RowCnt       = 0;   
   my @xlsWkSht3          = ();
   my $wkSht3RowCnt       = 0;

####### ------------------------------------------------------
# File Variables
#######  -----------------------------------------------------   
    my $iniFile;

#####################  Program starts here   ####################################
   my $old_stderr = *STDERR;
   
   OpenLogFile();
   OpenExceptionFile();   
   getINIFileParameters();
   
   printIniFileContents();
   logIt("************* Program Start Here *******************\n");
   
    if ($displayExceptionLog eq 1){
      close(OUTFILE);      
      OpenInputFileRead($LogFileDirectory,$ExceptionFileName);    
      printExceptionLogToConsole();
      close(INFILEREAD);
    };
 
     if ($displayLog eq 1){
      
      close(OUTFILE);
      OpenInputFileRead($LogFileDirectory,$LogFileName);     
      printLogToConsole();
      close(INFILEREAD);
    };   

    if ($displayLog eq 1 || $displayExceptionLog eq 1) {
      logIt("Printed Out Log Files\n");
      logIt("==> Closing Log file\n");
      close(OUTFILE);
      logException("==> Closing Exception file\n");
      close(ExceptionOUTFILE);
      exit(0);
    }
  
   Archive();
###  AMA logic
   RetrieveAMAData(($amaFileDir,$amaFile,"ama"));
   ProcessAMAHTML();
   SwapHTML("ama");
   close(OutHTMLReport);   

   InitializeArrays();
 
###  Resoucre logic   
   RetrieveAMAData(($resourceFileDir,$resourceFile,"resource"));
   ProcessAMAHTML();   
   SwapHTML("resource");
   close(OutHTMLReport);
   
   OpenExcelFile();
   LoadXLSXFile ();

  *STDERR  = $old_stderr;
   $workbook->close();  
   close(OUTHTMLMenu);
   AddLogFileHTML();
   AddArchiveFileHTML();
   SendEmailOut();
   logIt("==> Closing Log file\n");
   logIt("****------**** Program End Here ****-----****\n");  
   close(OUTFILE);
   logException("==> Closing Exception file\n");
   close(ExceptionOUTFILE);   
  
   print "\nWe are done!!\n";

   
#############################################   Sub routines    ##################################################


#############################**********************************#####################################
#                               Print out Log file to console

 sub printLogToConsole {
     logIt("*******------  Entering printLogToConsole module  ------*******\n"); 
     while (my $row = <INFILEREAD>) {
       chomp $row;
       print "$row\n";
     }
 }
 
 sub printExceptionLogToConsole {
        logIt("*******------  Entering printExceptionLogToConsole module  ------*******\n"); 
     while (<INFILEREAD>) {
       ##chomp $_;
       print $_;
     }
 } 

#############################**********************************#####################################
#                               INITIALIZE  SUB ROUTINES

sub Archive() {
     logIt("*******------  Entering Archive Archiving module  ------*******\n"); 
     ##  Archive the current ama.txt and resource.txt files into the archive directory
     if (-e $amaFileDir.$amaFile ) {
         MoveFile($amaFileDir.$amaFile,$archiveDirectory.$FileDatePrefix.$amaFile);
         logIt( "moving file: $amaFileDir$amaFile to $archiveDirectory$FileDatePrefix$amaFile\n");
     }
     
     if (-e $resourceFileDir.$resourceFile ) {
         MoveFile($resourceFileDir.$resourceFile,$archiveDirectory.$FileDatePrefix.$resourceFile);
         logIt( "moving file: $resourceFileDir$amaFile to $archiveDirectory$FileDatePrefix$resourceFile\n");
     }
     
     
     logIt("------   Starting AutoCrank  ------ \n");
     system(`perl ./AutoCrank.pl`);
     logIt("------   Completed AutoCrank  ------  \n");
    
     
     if (-e $htmlReportNameDir.$htmlAMAReportName ) {
         MoveFile($htmlReportNameDir.$htmlAMAReportName,$archiveDirectory.$FileDatePrefix.$htmlAMAReportName);
         logIt("Log File : $htmlReportNameDir$htmlAMAReportName  has been Archived\n")
        #print "moving file: $htmlReportNameDir$htmlAMAReportName to $archiveDirectory$FileDatePrefix$htmlAMAReportName";      
     }
     
     if (-e $htmlReportNameDir.$htmlResourceReportName ) {
         MoveFile($htmlReportNameDir.$htmlResourceReportName,$archiveDirectory.$FileDatePrefix.$htmlResourceReportName);
         logIt("Log File : $htmlReportNameDir$htmlResourceReportName  has been Archived\n")         
          #print "moving file: $htmlReportNameDir$htmlResourceReportName to $archiveDirectory$FileDatePrefix$htmlResourceReportName";
     }      
      
     if (-e $xlOutputDirectory.$xlFileName.".".$xlExtension ) {
         CopyFile($xlOutputDirectory.$xlFileName.".".$xlExtension,$archiveDirectory.$FileDatePrefix.$xlFileName.".".$xlExtension);
         logIt("Log File : $xlOutputDirectory$xlFileName"."$xlExtension  has been Archived\n")         
          #print "moving file: $htmlReportNameDir$htmlResourceReportName to $archiveDirectory$FileDatePrefix$htmlResourceReportName";
     }
          

     ##  archives the log files and the exception log files but it makes sure
     ##  that if the program is run multiple times on the same day it does not
     ##  archive today's file.  
     find(\&archiveLogInfo, $LogFileDirectory);
      sub archiveLogInfo() {
      ##   don't want to try to Archive the ARCHIVED files 
         if ($File::Find::dir.'/' eq $LogFileArchiveDirectory) {
            #code 
            next;
         }
               if (! -d $_){
      
                    if ($_ =~ /^.*\.txt$/)
                     {
                        my $tmpMonth = substr($_,0,2);
                        my $tmpDay   = substr($_,3,2);
                        my $tmpYear  = substr($_,6,4);
                        if ($tmpMonth eq $myMonth && $tmpDay eq $myDays && $tmpYear eq $myYear) {
                           next;
                        }else
                        {
                          if (-e $LogFileDirectory) {
                              if (-e $LogFileArchiveDirectory) {
                                 #code
                                 #####print("Current Dir/File: $File::Find::dir\n");
                                 #####print("Log file Dir: $LogFileDirectory\n");
                                 #####print("Log file Archive Dir: $LogFileArchiveDirectory\n")   ;                        
                                 MoveFile($LogFileDirectory.$_,$LogFileArchiveDirectory.$_);
                                 logIt("Log File : $LogFileDirectory.$_  has been Archived\n")
                              } else {
                                 logIt ("Log File Archive Directory doesn't exist\n");
                              }
      
                          } else
                           {
                              
                              logIt ("Log File Directory doesn't exist\n");
                           }
                        }
                     } else
                     {
                        next;
                     }

         } 
      }
 
###########   Archive Maintenance  -  remove them old files

        my $tmpTodayDate = time;    
        my $tmpModDate;
 
###  cleanse the ARchive Log and Exception directory
        # Open the PageCount.html archive directory.
         if (opendir (ARCHIVEDIR, $archiveDirectory))
         {    }
        else {
                $ErrorLocation      = "Sub ArchiveMaintenance";
                $ErrorReason        = "Archive - Unable to Open PageCount.html archive Directory: $archiveDirectory";
                $ErrorCode          = "$!";
                $EndProgram         = false;
                $ThereWasAnError    = true;                
                HandleErrors();
             } ; 
         # Read in the files.
         # You will not generally want to process the '.' and '..' files,
         # so we will use grep() to take them out.
         my @archiveFiles = grep { !/^\.{1,2}$/ } readdir (ARCHIVEDIR);
         closedir (ARCHIVEDIR);
         
         for (@archiveFiles ) {
            if (-d $_) {
               next;
            }

            my $myFile = $_;
            my $sb = stat($archiveDirectory.$myFile);
            $tmpModDate = ($sb->mtime);

            my $tmpDiffamt = ((($HTMLArchiveDays *24) * 60) * 60);
            if (($tmpModDate) < ($tmpTodayDate - $tmpDiffamt)) {
               unlink $archiveDirectory.$myFile;
               logIt("Removing archive file $archiveDirectory.$_\n");

            }

         }
         

###  cleanse the ARchive Log and Exception directory
     
        # Open the Log.html directory.
         if (opendir (ARCHIVEDIR, $LogFileArchiveDirectory))
         {  @archiveFiles  = (); }
        else {
                $ErrorLocation      = "Sub ArchiveMaintenance";
                $ErrorReason        = "Archive - Unable to Open Log file Directory: $LogFileArchiveDirectory";
                $ErrorCode          = "$!";
                $EndProgram         = false;
                $ThereWasAnError    = true;                
                HandleErrors();
             } ; 
         # Read in the files.
         # You will not generally want to process the '.' and '..' files,
         # so we will use grep() to take them out.
         @archiveFiles = grep { !/^\.{1,2}$/ } readdir (ARCHIVEDIR);
         closedir (ARCHIVEDIR);
         
         for (@archiveFiles ) {
             if (-d $_) {
               next;
             }
             
             $myFile = $_;
             $sb = stat($LogFileArchiveDirectory.$myFile);
             $tmpModDate = ($sb->mtime);
           
            if ($tmpModDate < ($tmpTodayDate - ((($LogArchiveDays *24) * 60) * 60))) {
               unlink $LogFileArchiveDirectory.$myFile;
               logIt("Removing archive file $LogFileArchiveDirectory.$_\n");

            }
         }      

}

#############################***********Read Ini File Parameters*************#####################################  
#
#  Read in the input Parameter iniFile identified by the variable $LocalINIFile
#  this user-editable file contains the user-specified criteria for Raddon files
#  The data is read from the file and loaded into Global variables to be used
#  through out the program
#
sub getINIFileParameters() {
  logIt("*******------  Entering getINIFileParameters module  ------*******\n");   
 if (-e $LocalINIFile) {
    #print "$programName  Input file found: $LocalINIFile\n";
    if (-r $LocalINIFile) {
        #print "$programName  Input file readable\n";
    } else {
	    $logdieExit = "171";         
            #print "$programName  ERROR - Input file $LocalINIFile NOT readable\n";
            $ErrorLocation      = "Sub getINIFileParameters";
            $ErrorReason        = "Input file $LocalINIFile NOT readable: $LocalINIFile";
            $ErrorCode          = "$!";
            $EndProgram         = true;
            $ThereWasAnError    = true;          
            HandleErrors();            
    }    
   } else {
            $logdieExit = "172";
            #print "$programName  ERROR - Input file $LocalINIFile NOT found\n";
            $ErrorLocation      = "Sub getINIFileParameters";
            $ErrorReason        = "Input file NOT found: $LocalINIFile";
            $ErrorCode          = "$!";
            $EndProgram         = true;
            $ThereWasAnError    = true;          
            HandleErrors();           

   }

   $iniFile = Config::IniFiles->new( -file => $LocalINIFile );

   $development                              = $iniFile->val('DIRECTORY_NAME','Development');   

   if ($development eq "true") {
    ###  read ini file and get parameter values  

            $xlOutputDirectory               = $programPath.$iniFile->val('DIRECTORY_NAME','xlOutputDirectory');
            $LogFileArchiveDirectory         = $programPath.$iniFile->val('DIRECTORY_NAME','LogFileArchiveDirectory');

            $outHTMLMenuDir                  = $programPath.$iniFile->val('DIRECTORY_NAME','outHTMLMenuDir');
            $htmlReportNameDir               = $programPath.$iniFile->val('DIRECTORY_NAME','htmlReportNameDir');
            $archiveDirectory                = $programPath.$iniFile->val('DIRECTORY_NAME','archiveDirectory');
            $amaFileDir                      = $programPath.$iniFile->val('DIRECTORY_NAME','amaFileDir');
            $resourceFileDir                 = $programPath.$iniFile->val('DIRECTORY_NAME','resourceFileDir');   
            
            
            $outHTMLMenu                     = $iniFile->val('FILE_SPECIFICS','outHTMLMenu');
            $htmlOriginalReportName          = $iniFile->val('FILE_SPECIFICS','htmlOriginalReportName');
            $htmlTemplateSelectorName        = $iniFile->val('FILE_SPECIFICS','htmlTemplatePageSelector');
            $htmlSelectorName                = $iniFile->val('FILE_SPECIFICS','htmlPageSelector');
            $htmlTemplateArchivePageSelector = $iniFile->val('FILE_SPECIFICS','htmlTemplateArchivePageSelector');
            $htmlArchivePageSelector          = $iniFile->val('FILE_SPECIFICS','htmlArchivePageSelector');
            $htmlAMAReportName               = $iniFile->val('FILE_SPECIFICS','htmlAMAReportName');
            $htmlResourceReportName          = $iniFile->val('FILE_SPECIFICS','htmlResourceReportName');
            
            $amaFile                         = $iniFile->val('FILE_SPECIFICS','amaFile');
            $resourceFile                    = $iniFile->val('FILE_SPECIFICS','resourceFile');   
            
            $htmlUseLink                     = $iniFile->val('FILE_SPECIFICS','htmlUseLink');
            $xlFileName                      = $iniFile->val('FILE_SPECIFICS','xlFileName');
   
      } else
   {
            $xlOutputDirectory               = $programPath."Output/";
            $LogFileArchiveDirectory         = $programPath."Output/Logfiles/Archive/";

            $outHTMLMenuDir                  = $programPath."Output/";
            $htmlReportNameDir               = $programPath."Output/";
            $archiveDirectory                = $programPath."Output/Archive/";
            $amaFileDir                      = $programPath."ama/";
            $resourceFileDir                 = $programPath."resource/";   
            
            
            $outHTMLMenu                     = "outHTMLMenu.html";
            $htmlOriginalReportName          = "TemplatePageCounter.html";
            $htmlTemplateSelectorName        = "TemplatePageSelector.html";
            $htmlSelectorName                = "PageSelector.html";
            
            $htmlTemplateArchivePageSelector = "TemplateArchivePageSelector.html";
            $htmlArchivePageSelector         = "ArchivePageSelector.html";

            $htmlAMAReportName               = "PageCounterAMA.html";
            $htmlResourceReportName          = "PageCounterResource.html";
            
            $amaFile                         = "ama.txt";
            $resourceFile                    = "resource.txt";   
            
            $htmlUseLink                     = $iniFile->val('FILE_SPECIFICS','htmlUseLink');
            $xlFileName                      = "PageCount";
  
   }
   
   $xlExtension                     = $iniFile->val('FILE_SPECIFICS','xlExtension');
   $worksheet_1                     = $iniFile->val('FILE_SPECIFICS','worksheet_1');
   $wkSht1Col1                      = $iniFile->val('FILE_SPECIFICS','wkSht1Col1');
   $wkSht1Col2                      = $iniFile->val('FILE_SPECIFICS','wkSht1Col2');
   $wkSht1Col3                      = $iniFile->val('FILE_SPECIFICS','wkSht1Col3');
   $wkSht1Col4                      = $iniFile->val('FILE_SPECIFICS','wkSht1Col4');
   $worksheet_2                     = $iniFile->val('FILE_SPECIFICS','worksheet_2');
   $wkSht2Col1                      = $iniFile->val('FILE_SPECIFICS','wkSht2Col1');
   $wkSht2Col2                      = $iniFile->val('FILE_SPECIFICS','wkSht2Col2');
   $wkSht2Col3                      = $iniFile->val('FILE_SPECIFICS','wkSht2Col3');
   $wkSht2Col4                      = $iniFile->val('FILE_SPECIFICS','wkSht2Col4');
   $worksheet_3                     = $iniFile->val('FILE_SPECIFICS','worksheet_3');
   $wkSht3Col1                      = $iniFile->val('FILE_SPECIFICS','wkSht3Col1');
   $wkSht3Col2                      = $iniFile->val('FILE_SPECIFICS','wkSht3Col2');
   $wkSht3Col3                      = $iniFile->val('FILE_SPECIFICS','wkSht3Col3');


   $HTMLArchiveDays                 = $iniFile->val('FILE_SPECIFICS','HTMLArchiveDays');
   $LogArchiveDays                  = $iniFile->val('FILE_SPECIFICS','LogArchiveDays');
   
   
   $recipient                       = $iniFile->val('EMAIL','Recipient');
   $errorRecipient                  = $iniFile->val('EMAIL','RecipientError');
   $fromSender                      = $iniFile->val('EMAIL','FromSender');
   $errorEmail                      = $iniFile->val('EMAIL','ErrorEmail');
   $emailMessage                    = $iniFile->val('EMAIL','EmailMessage');
   $emailSubject                    = $iniFile->val('EMAIL','EmailSubject');
   $errorEmailMessage               = $iniFile->val('EMAIL','ErrorEmailMessage');
   $errorEmailSubject               = $iniFile->val('EMAIL','ErrorEmailSubject'); 
}
#############################**********************************#####################################
#  printIniFileContents
#   ummmm, prints out the Ini File Contents to the console- duh!!
#
 sub printIniFileContents() {
     logIt("*******------  Entering printIniFileContents module  ------*******\n");
     
     logIt("------  Start of Configuration File Settings  ------\n");        
  # Log all input parameters
  logIt ("INI File Used             : $LocalINIFile\n");

  logIt ("SECTION  DIRECTORY_NAME\n");
 
  logIt (" Development              : $development \n");
  logIt (" Application Directory    : $programPath \n");  
  logIt (" LogFileDirectory         : $LogFileDirectory\n");
  logIt (" LogFileArchiveDirectory  : $LogFileArchiveDirectory\n");  
  logIt (" xlOutputDirectory        : $xlOutputDirectory\n");
  logIt (" outHTMLMenuDir           : $outHTMLMenuDir\n");   
  logIt (" htmlReportNameDir        : $htmlReportNameDir\n");
  logIt (" archiveDirectory         : $archiveDirectory\n");
  
  logIt (" amaFileDir               : $amaFileDir\n");
  logIt (" resourceFileDir          : $resourceFileDir\n");   

  logIt ("SECTION  FILE_SPECIFICS\n");
  logIt (" HTMLArchiveDays          : $HTMLArchiveDays\n");
  logIt (" LogArchiveDays           : $LogArchiveDays\n");
  logIt (" outHTMLMenu              : $outHTMLMenu\n"); 
  logIt (" htmlOriginalReportName   : $htmlOriginalReportName\n");
  logIt (" htmlTemplatePageSelector : $htmlTemplateSelectorName\n");
  logIt (" htmlPageSelector         : $htmlSelectorName\n");
  logIt (" htmlAMAReportName        : $htmlAMAReportName\n");
  logIt (" htmlResourceReportName   : $htmlResourceReportName\n");  
  
  
  logIt (" amaFile                  : $amaFile\n");
  logIt (" resourceFile             : $resourceFile\n");   
  
  logIt (" htmlUseLink              : $htmlUseLink\n");  
  logIt (" xlFileName               : $xlFileName\n");
  logIt (" xlExtension              : $xlExtension\n");
  logIt (" worksheet_1              : $worksheet_1\n");
  logIt (" wkSht1Col1               : $wkSht1Col1\n");  
  logIt (" wkSht1Col2               : $wkSht1Col2\n");  
  logIt (" wkSht1Col3               : $wkSht1Col3\n");  
  logIt (" wkSht1Col4               : $wkSht1Col4\n");
  
  logIt (" worksheet_2              : $worksheet_2\n");
  logIt (" wkSht2Col1               : $wkSht2Col1\n");
  logIt (" wkSht2Col2               : $wkSht2Col2\n");  
  logIt (" wkSht2Col3               : $wkSht2Col3\n");  
  logIt (" wkSht2Col4               : $wkSht2Col4\n");  
   
  logIt (" worksheet_3              : $worksheet_3\n");
  logIt (" wkSht3Col1               : $wkSht3Col1\n");
  logIt (" wkSht3Col2               : $wkSht3Col2\n");
  logIt (" wkSht3Col3               : $wkSht3Col3\n");  
  
  
  logIt (" ReportDelimiter          : $ReportDelimiter\n");  
  logIt (" HTMLArchiveDays          : $HTMLArchiveDays\n");
  logIt (" LogArchiveDays           : $LogArchiveDays\n");    
  
  logIt ("SECTION  EMAIL\n");
  logIt (" Recipient                : $recipient\n");
  logIt (" RecipientError           : $errorRecipient\n");
  logIt (" FromSender               : $fromSender\n");
  logIt (" ErrorEmail               : $errorEmail\n");    
  logIt (" EmailMessage             : $emailMessage\n");  
  logIt (" EmailSubject             : $emailSubject\n");     
  logIt (" ErrorEmailMessage        : $errorEmailMessage\n");     
  logIt (" ErrorEmailSubject        : $errorEmailSubject\n");  

  logIt("------  End of Configuration File Settings  ------\n");    
 }
 
 
#############################**********************************#####################################
#  this will read in the Ama.txt file into @amaInArray; 
#  @directoryNameArray is then built having the name of the Directory for each of the array elements in ( in [1])
#     in @amaInArray and their array location(in [0]); [2] = count of pages;[3] modified date; [4] isDirectory
#  @sortedDirNameArray sorts @directoryNameArray using the directory names - this will align all directories in alpha order
#     this is an array of hashes that contain: [0] Directory name;  [1] amaInArray location; [2] Total file count
#     the count in [2] will only be placed into the first occurance of the directory name, which is the actual
#     directory listing and all of the remaining ones that have the identical directory name will be blank
#     because these are pages....also, the count is a cascading count where it counts not only an identical
#     directory name but also directory names that are sub directories of this one.
#  
#

   sub RetrieveAMAData {
      logIt("*******------  Entering RetrieveAMAData module  ------*******\n"); 

      @amaInArray = ();
      my ($tmpDir,$tmpFile,$tmpFileType) = @_;
      ## open the text file that contains the AMA directory information  
      OpenInputFileRead($tmpDir,$tmpFile);
      ## load txt file into array for processing...
      @amaInArray = <INFILEREAD>;
      my @sortedAMAInArray = sort {"\L$a" cmp "\L$b"} @amaInArray;
      
      my $tmp2 = @sortedAMAInArray;
      ##print "SORTED-AMA-IN-ARRAY\n";
      ##print "--------------------------------------\n";
      ##for (my $tmp1 = 0;$tmp1 < $tmp2; $tmp1++ ){
      ##   print $sortedAMAInArray[$tmp1];
      ##}
      
      my $tmpArrayCnt = @sortedAMAInArray;
      ##print "Number of Files from AMA In file:  $tmpArrayCnt\n";

      my $tmpNDX            = 0;
      my $xElement          = "";
      my $xElement1          = "";      
      my $modDate           = "";
      my $xCnt              = 0;
      my $matchCount        = 0;
      my $lastElement       = "";
      my $lastMatchLocation = 0;
      my $isDirectory       = 1;
      my $individualCnt     = 0;
      my @tmpBizUnit        = ();
      my $tmpBizUnitCnt     = 0;
      my $bizUnit           = "";
      my $tmpPermission     = "";
      my $tmpUrl            = "http://";
      my $tmpSortedArray    = "";
      
      ##########################print scalar(localtime(time))."\n";
      
      
     #####  for($xCnt = 0; $xCnt < 100 ;$xCnt++){
     for($xCnt = 0; $xCnt < $tmpArrayCnt;$xCnt++){
         
         $tmpNDX = rindex($sortedAMAInArray[$xCnt],'-');         
         $modDate  = substr($sortedAMAInArray[$xCnt],$tmpNDX - 7,11);
         $tmpNDX = rindex($sortedAMAInArray[$xCnt],'/');
         $xElement = substr($sortedAMAInArray[$xCnt],0,$tmpNDX+1);
         @tmpBizUnit = split /\//,($xElement);

         $tmpBizUnitCnt = @tmpBizUnit;
         
      $tmpSortedArray = substr( $sortedAMAInArray[$xCnt],0,length($sortedAMAInArray[$xCnt]) - 17);      
         
      if ($tmpFileType eq "ama") {           
         
         if ($tmpBizUnitCnt > 2) {
             $bizUnit = $tmpBizUnit[2];
             $tmpPermission = "";
         }
          if ($tmpBizUnitCnt > 3) {
             $tmpPermission = $tmpBizUnit[3];
         }
      } else
     {   

         if ($tmpBizUnitCnt > 3) {
             $bizUnit = $tmpBizUnit[3];
             $tmpPermission = "";
         }
          if ($tmpBizUnitCnt > 4) {
             $tmpPermission = $tmpBizUnit[4];
         }
     }
    
    
         #########################$tmpTitle = ExtractTitle($tmpUrl.(split / /,$sortedAMAInArray[$xCnt])[0]);
         
        ####################print scalar(localtime(time))." Returned from Title  \n";
               
               
        if ($tmpFileType eq "ama") {       

            $xlsWkSht1[$wkSht1RowCnt][0]  = $bizUnit;
            $xlsWkSht1[$wkSht1RowCnt][1]  = $tmpPermission;  
            $xlsWkSht1[$wkSht1RowCnt][2]  = $modDate;          
            $xlsWkSht1[$wkSht1RowCnt][3]  = $tmpSortedArray;
         
            $wkSht1RowCnt++;                 
               
        } else
        {

            $xlsWkSht2[$wkSht2RowCnt][0]  = $bizUnit;                       
            $xlsWkSht2[$wkSht2RowCnt][1]  = $tmpPermission;  
            $xlsWkSht2[$wkSht2RowCnt][2]  = $modDate;          
            $xlsWkSht2[$wkSht2RowCnt][3]  = $tmpSortedArray;
           
            $wkSht2RowCnt++;
         }
         
         
         
         #####################print "Count is: $xCnt  BizUnit is : $bizUnit Title is:  $tmpTitle \n";           
         #################print scalar(localtime(time))."\n";
         push @directoryNameArray,{arrayLoc=>$xCnt,arrayElement=>$xElement,dirCount=>0,fileCount=>0,modDate=>$modDate,isDirectory=>0,bizUnit=>$bizUnit,permission=>$tmpPermission};
      }
      
      
        #########################print scalar(localtime(time))."\n";
      
     ##  sorts the array of hashes into Directory sort order.  this will give me
     ##   an alphabetized list of Directories (element 2) and their location on the
     ##   amaInArray (element 1)  
    # @sortedDirNameArray = sort{$a->{arrayElement} cmp $b->{arrayElement}} @directoryNameArray;
    
    
     $tmp2 = @directoryNameArray;
      ###print "DIRECTORY-NAME-ARRAY\n";
      ###print "--------------------------------------\n";
      ###for ( $tmp1 = 0;$tmp1 < $tmp2; $tmp1++ ){
      ###   print "loc: $directoryNameArray[$tmp1]->{arrayLoc} ";
      ###   print "ele: $directoryNameArray[$tmp1]->{arrayElement} ";
      ###   print "dir: $directoryNameArray[$tmp1]->{dirCount} ";
      ###   print "fil: $directoryNameArray[$tmp1]->{fileCount} ";
      ###   print "dat: $directoryNameArray[$tmp1]->{modDate} ";
      ###   print "dat: $directoryNameArray[$tmp1]->{bizUnit} \n";
      ###   print "dat: $directoryNameArray[$tmp1]->{permission} \n";
      ###}

     @sortedDirNameArray = sort{$a->{arrayElement} cmp $b->{arrayElement}} @directoryNameArray;

     $tmp2 = @sortedDirNameArray;
      ####print "SORTED-NAME-ARRAY\n";
      ####print "--------------------------------------\n";
      ####for ( $tmp1 = 0;$tmp1 < $tmp2; $tmp1++ ){
      ####   print "loc: $sortedDirNameArray[$tmp1]->{arrayLoc} ";
      ####   print "ele: $sortedDirNameArray[$tmp1]->{arrayElement} ";
      ####   print "dir: $sortedDirNameArray[$tmp1]->{dirCount} ";
      ####   print "fil: $sortedDirNameArray[$tmp1]->{fileCount} ";
      ####   print "dat: $directoryNameArray[$tmp1]->{modDate} ";
      ####   print "unt: $directoryNameArray[$tmp1]->{bizUnit} \n";           
      ####   print "per: $directoryNameArray[$tmp1]->{permission} \n";   
      ####  
      ####}      

      $tmpArrayCnt = @sortedDirNameArray;
      
      my $innerCnt = 0;
      my $keepTesting    = true;
      for ($tmpNDX = 0; $tmpNDX < $tmpArrayCnt; $tmpNDX++){
       
         $xElement = $sortedDirNameArray[$tmpNDX]->{arrayElement};
         if ($xElement eq $lastElement) {
            next;
         }
         ##  don't want to repeat the check for the same
         ##  directory name over and over again being the
         ##  array of hashes has duplicate values
         $lastElement       = $xElement;
         ## this is the location of the Directory
         ##  where i want to write the total count
         $lastMatchLocation = $tmpNDX;
         $keepTesting       = true;
         ##  you want to start the testing with the current location
         ##  otherwise the first check will NOT match unless this is
         ## the first time through
         $innerCnt    = $tmpNDX;

         $matchCount = 0;
         while ($keepTesting eq true) {
         
               if ($xElement =~ /$sortedDirNameArray[$innerCnt]->{arrayElement}/  and length($sortedDirNameArray[$innerCnt]->{arrayElement}) > 0) {
                  $matchCount++;
                  $innerCnt++;
                  
               } else
               {
                  ## you should update the directory count
                  ##   using this location $lastMatchLocation
                  ##   and the $matchCount value
                  $sortedDirNameArray[$lastMatchLocation]->{fileCount}=$matchCount - 1;
                 
                  $keepTesting = false;
               }
         }           
         
         $innerCnt    = $tmpNDX;         
         $matchCount  = 0;
         $keepTesting = true;         
               
         while ($keepTesting eq true) {
               if (index($sortedDirNameArray[$innerCnt]->{arrayElement},$xElement) > -1) {                      
                  #code
                  $matchCount++;
                  $innerCnt++;
               } else
               {
               
                  ## you should update the directory count
                  ##   using this location $lastMatchLocation
                  ##   and the $matchCount value
                  $sortedDirNameArray[$lastMatchLocation]->{dirCount}=$matchCount;
                  $sortedDirNameArray[$lastMatchLocation]->{isDirectory}=$isDirectory;                  
                  $keepTesting = false;
                  next;
               }
         }
      }
## now is the time to build the final array that has all of the
## directory names in alpha order with all of their associated data

##   @finalAMAArray will house the data
##  the @sortedDirNameArray is an array of hashes with each hash having
##   the following  arrayLoc, arrayElement, dirCount, modDate


      $tmpNDX      = 0;
      $tmpArrayCnt = @sortedDirNameArray;
      $isDirectory = 0;
      $matchCount  = 0;
      $xElement    = "";
      $innerCnt    = 0;
      $modDate     = 0;
      my $outerArrayRow = 0;
      my $arrayRow = 0;
      my $fileCnt  = 0;
  
      for($xCnt = 0; $xCnt < $tmpArrayCnt;$xCnt++)
      {
         $tmpNDX      = $sortedDirNameArray[$xCnt]->{arrayLoc};
         $isDirectory = $sortedDirNameArray[$xCnt]->{isDirectory};
         if ($isDirectory eq 1) {
            ### we want to pull all of the array elements that belong to this
            ### directory and place them into a mini array of hashes
            ### and then load that array of hashes into the finalArray
            ### so when creating the HTML doc it will read from this
            ### this mini just like it would read from a directory.
            # discover how many rows belong to this directory
            $matchCount = $sortedDirNameArray[$xCnt]->{dirCount};
            #  create a clean mini array
            my @miniDirArray = ();
            # discover how many files this directory has so we know how many times to read
            ##  the array and load the miniarray with the rows of file names
            $fileCnt = $sortedDirNameArray[$xCnt]->{fileCount};
            # set the mini row counter to the current row 
            $innerCnt = $xCnt;
            $arrayRow = 0;
            for (my $loopCnt = 0; $loopCnt <= $fileCnt; $loopCnt++){
                   
                  $xElement = (split / /,$sortedAMAInArray[$sortedDirNameArray[$innerCnt]->{arrayLoc}])[0];                 
   
                  $modDate  = $sortedDirNameArray[$innerCnt]->{modDate};
                  
                  $miniDirArray[$arrayRow][0] = $sortedDirNameArray[$innerCnt]->{isDirectory};
                  $miniDirArray[$arrayRow][1] = $xElement;
                  $miniDirArray[$arrayRow][2] = $sortedDirNameArray[$innerCnt]->{modDate};
                  $miniDirArray[$arrayRow][3] = $sortedDirNameArray[$innerCnt]->{dirCount};
                  $miniDirArray[$arrayRow][4] = $sortedDirNameArray[$innerCnt]->{fileCount};
                  $miniDirArray[$arrayRow][5] = $sortedDirNameArray[$innerCnt]->{bizUnit};
                  $miniDirArray[$arrayRow][6] = $sortedDirNameArray[$innerCnt]->{permission};
                  
                  $innerCnt++;
                  $arrayRow++;
            }
            @finalAMAArray[$outerArrayRow] = [@miniDirArray];
            $outerArrayRow++;
     
         } else
         {
            $matchCount = $sortedDirNameArray[$xCnt]->{modDate};
         }
      }
 
   }

 
#############################**********************************#####################################
#  InitializeArrays
#


 sub InitializeArrays {
   logIt("*******------  Entering InitializeArray module  ------*******\n");    

    @amaInArray         = (); 
    @directoryNameArray = ();
    @sortedDirNameArray = ();
    @finalAMAArray      = ();
    
    $xlsWkSht3[$wkSht3RowCnt][0]  = "";
    $xlsWkSht3[$wkSht3RowCnt][1]  = "";
    $xlsWkSht3[$wkSht3RowCnt][2]  = "";          
    $wkSht3RowCnt++;    
    
    $xlsWkSht3[$wkSht3RowCnt][0]  = "";
    $xlsWkSht3[$wkSht3RowCnt][1]  = "RESOURCE/DOC";
    $xlsWkSht3[$wkSht3RowCnt][2]  = "";          
    $wkSht3RowCnt++;        

    $xlsWkSht3[$wkSht3RowCnt][0]  = "";
    $xlsWkSht3[$wkSht3RowCnt][1]  = "";
    $xlsWkSht3[$wkSht3RowCnt][2]  = "";          
    $wkSht3RowCnt++;     
    
 }
 
##################################**********************************#####################################
######  this will go through the $finalAMAArray structure and is used 
######  to build the HTML output file.  It will create the UL LI relationship 
######  for the folders/folders or folders/directories.  The file will be built
######  having the proper <li> or <ul> necessary for printihg the report
######
      sub ProcessAMAHTML
      {
        ### next var is used for the <li id=   value to make each iteration unique
       logIt("*******------  Entering ProcessAMAHtml module  ------*******\n");         
        my $liChildCntr = 0;
      
        my $liSelector1    = "<li> <a href='javascript:void(0);' childid = 'pageCounter_";
        my $liSelector2    = "' class='cat_close category'>&nbsp;&nbsp;&nbsp;&nbsp;";
        my $liFileSelector = "<li> <a href='javascript:void(0);' class='product'>&nbsp;&nbsp;&nbsp;&nbsp;";
        my $ulSelector1    = "<ul id='pageCounter_";
        my $ulSelector2    = "'>";

           $arrayRowCnt    = 0;
        my @workingArray   = ();
        
         my $isDirectory     =  0;            
         my $fullPath        =  "";
         my $fullDir         =  "";
         my $modDate         =  0;            
         my $cascadeCount    =  0;
         my $individualCount =  0;                 

        ## opens the output HTML ul / il menu file
         OpenHTMLMenuOut;
         
         WriteHTMLContents("<ul id='root' class='menu'>\n");
         ### do the first Array read to get the starting directory information
         
         $lastDirName = "";
         
         for (@finalAMAArray)             
         {
            
            $isDirectory     =  $_->[0]->[0];            
            $fullDir         =  $_->[0]->[1];            
            $modDate         =  $_->[0]->[2];            
            $cascadeCount    =  $_->[0]->[3];
            $individualCount =  $_->[0]->[4];

            # If the file is a directory
             if ($isDirectory eq 1){
 
                     
                  $xlsWkSht3[$wkSht3RowCnt][0] = $cascadeCount;   
                  $xlsWkSht3[$wkSht3RowCnt][1] = $individualCount;   
                  $xlsWkSht3[$wkSht3RowCnt][2] = $fullDir;    
                  $wkSht3RowCnt++;   
      
                   @lastDirArray = split (/\//,$lastDirName);
                   @curDirArray  = split (/\//,$fullDir);
                   $lastDirCnt   = @lastDirArray;
                   $curDirCnt    = @curDirArray;
                   
                   if ($lastDirCnt eq $curDirCnt) {
                        WriteHTMLContents("</ul>\n");
                   } else {
                   
                   if ($lastDirCnt > $curDirCnt) {
                     for (my $checkNdx = 0; $checkNdx < $lastDirCnt; $checkNdx++) {
                        if ($lastDirArray[$checkNdx] eq $curDirArray[$checkNdx]) {
                           #code
                        } else
                        {
                           WriteHTMLContents("</ul>\n");
                        }
                     }
                  
                   ##  maybe this next section is not important because
                   ##  if it falls here then the current dir is probably a child dir.
                   }
                   else
                     {
                         #####WriteHTMLContents("$ulSelector1$liChildCntr$ulSelector2\n");
                       
                     }
                   }
                   $cascadeCount--;
                   
                  WriteHTMLContents("$liSelector1$liChildCntr$liSelector2&nbspCascade Cnt:&nbsp$cascadeCount&nbsp;&nbspFile Cnt:&nbsp$individualCount&nbsp;&nbsp;&nbsp$fullDir</a>\n");
                                  ##" '<ul'      'pageCounter_X' '>'"
                  WriteHTMLContents("$ulSelector1$liChildCntr$ulSelector2\n");
                   if ($individualCount > 0) {
                        for (my $pagePrinter = 1;$pagePrinter <= $individualCount; $pagePrinter++){
                             $fullPath   = $_->[$pagePrinter]->[1];
                             $modDate    = $_->[$pagePrinter]->[2];
                             WriteHTMLPage($fullPath,$modDate);
                        }
     
                   }
                    
                  ##  this is used only to make the ul / li unique...                            
                  $liChildCntr++;
   
                     
                  $lastDirName  = $fullDir;                   
                  
            }
             else {

                  ####do nothing here
                  }
            }
          WriteHTMLContents("</ul>\n");         
         }

      sub WriteHTMLPage() {
             my $liFileSelector = "<li> <a href='javascript:void(0);' class='product'>&nbsp;&nbsp;&nbsp;&nbsp;";
              my ($tmpPath, $tmpModDate) = @_;
              
              WriteHTMLContents("$liFileSelector$tmpPath"."&nbsp;&nbsp =>&nbsp;&nbsp; $tmpModDate</a></li>\n");
        
      }
 
 
#############################**********************************#####################################
#  this will look into the pageCounter_Original.html and find the <div class="content">
#     this is the start indicator for the replace text and it continues until first </div>
#     that follow the UL LI menu structure.  these are the beginning and ending markers for  
#     where the block of code should be swapped out with the new block of html ul / li code
#     that was generated with this run of the program

sub SwapHTML() {
     logIt("*******------  Entering SwapHTML module  ------*******\n");    


     close(OUTHTMLMenu);     
     OpenHtmlMenuIn();
     OpenHtmlIn();
     OpenHtmlOut(@_);
     
     my $tmpFileDate = substr($FileDatePrefix,0,length($FileDatePrefix)-1);

    logIt("-----  SwapHTML entering While Loop  -----\n");     
     #  now the htmlMenu file where all of the UL and LI rows have been written to is opened for input
     #  also, the HTML original file, which is the template html file that has the <div> Replace me in it is opened for input
     #  finally a new file, the HTML report file that will be a mash-up of the template and menu files is opened for output
     while (<INHTMLReport>) {
        if (/RUN_DATE_HERE/) {
          print OutHTMLReport "<p> Run Date: </p><p> $tmpFileDate </p>\n";
          
          
          ####print OutHTMLReport "<p> Run Date: $FileDatePrefix </p>\n";
          
          
        } else {
        if (/REPLACE_ME/) {
          while (<INHTMLMenu>) {
            print OutHTMLReport $_;              
          } 
        } else
        {
          print OutHTMLReport $_;
          
        }
     }
     };
}

#############################**********************************#####################################
#  this will look into the pageCounter_Original.html and find the <div class="content">
#  

sub AddLogFileHTML() {
    logIt("*******------  Entering AddLogFileHTML module  ------*******\n");    

     OpenLogFileHtmlIn();
     OpenHtmlLogFileOut();
   
    logIt("-----  AddLogFileHTML entering While Loop  -----\n");     
     #  now the htmlMenu file where all of the UL and LI rows have been written to is opened for input
     #  also, the HTML original file, which is the template html file that has the <div> Replace me in it is opened for input
     #  finally a new file, the HTML report file that will be a mash-up of the template and menu files is opened for output
     while (<INHTMLLOGFILE>) {

        if (/LOG FILE HERE/) {
         print OutHTMLLOGFILE "<li>  <a href=./Logfiles/".$LogFileName.">Log File</a> </li>\n";
	 print OutHTMLLOGFILE "	<li> <a href=#></a> </li>\n";
         print OutHTMLLOGFILE " <li>  <a href=./Logfiles/".$ExceptionFileName.">Exception File</a> </li>\n";
        }
         else
        {       
          print OutHTMLLOGFILE $_;
          
        }

     };
}


#############################**********************************#####################################
#  builds the Archive Page Selector . html file
#  

sub AddArchiveFileHTML() {
    logIt("*******------  Entering AddArchiveFileHTML module  ------*******\n");    

     OpenArchiveFileHtmlIn();
     OpenHtmlArchiveFileOut();
     
     
      if (! opendir (ARCHIVEDIR, $archiveDirectory))
         {
             $ErrorLocation      = "Sub AddArchiveFileHTML";
             $ErrorReason        = "Archive - Unable to Open PageCount.html archive Directory: $archiveDirectory";
             $ErrorCode          = "$!";
             $EndProgram         = false;
             $ThereWasAnError    = true;                
             HandleErrors();
          } ; 
         # Read in the files.
         # You will not generally want to process the '.' and '..' files,
         # so we will use grep() to take them out.
         #  and only grap specific files
        my @archiveFiles =  grep { !/^\.{1,2}$/ && (/\.html$/ || /\.txt$/ || /\.xlsx$/) } readdir (ARCHIVEDIR);         
        my @ulArchiveLinks = map ( "<li>  <a href=./Archive/".$_.">$_</a> </li>\n", reverse(sort(@archiveFiles)) ) ;         
        my $tmpCombo;         

   logIt("-----  AddArchiveFileHTML entering While Loop  -----\n");     
     #  now the htmlMenu file where all of the UL and LI rows have been written to is opened for input
     #  also, the HTML original file, which is the template html file that has the <div> Replace me in it is opened for input
     #  finally a new file, the HTML report file that will be a mash-up of the template and menu files is opened for output
     while (<INHTMLARCHIVEFILE>) {
        if (/ARCHIVE FILE HERE/) {
            foreach $tmpCombo (@ulArchiveLinks){
                 print OutHTMLARCHIVEFILE $tmpCombo;
                 
            }          
        }
         else
        {       
          print OutHTMLARCHIVEFILE $_;
          
        }
     };
     
     close(OutHTMLARCHIVEFILE);
}


#############################**********************************#####################################
#  open the file and parse rows until the <title>  row is located
#    then 
#

sub ExtractTitle() {
    logIt("*******------  Entering ExtractTitle module  ------*******\n"); 
         ####print scalar(localtime(time))." get URL  \n";
     my ($url) = @_;
     my $MatchFound  = false;
     my $fileTitle   = "";
     my $titlePos    = 0;
     my $endTitlePos = 0;
     
    # my $url = "http://www.ama-assn.org/ama/home.page";
     ####my $url = "http://www.ama-assn.org/ama/no-index/emergency-app-downloadbb.page";
    
    ################    <title>AMA Error Page</title>    
     my $content = get($url);
                   #####print scalar(localtime(time))."  Find Match  \n";
               $titlePos    = index($content,"<title>");
               $endTitlePos = index($content,"</title>");
               if (($titlePos > 0) and ($endTitlePos > 0)) {
                          ####print scalar(localtime(time))."  Match Found \n";
                    $MatchFound = true;
                    $xElement = substr($content,$titlePos+7,$endTitlePos - ($titlePos + 7));
                    return $xElement;

               }
               

          if (! $MatchFound) {

          $ErrorReason        = "Unable to locate this Title";
                  ####print scalar(localtime(time))."  Match NOT found  \n";
          return $ErrorReason;
      
            }
        };


#############################**********************************##################################### 
#  copies the originalPageCounter.html file to be PageCounter.html
#  
#
 sub CopyFile()
 {
   logIt("*******------  Entering CopyFile module  ------*******\n");    
    my ($WorkingFileName,$NewFileName) = @_; 
    if (! -e $WorkingFileName) {
          $ErrorLocation = "Sub CopyFile"; 
          $ErrorReason   = "$WorkingFileName can not be found\n";
          $ErrorCode     = " $!";
          $EndProgram    = true;
          $ThereWasAnError = true;
          HandleErrors();
    }
    
    if (! copy ( $WorkingFileName, $NewFileName)) 
    {

          $ErrorLocation = "Sub CopyFile"; 
          $ErrorReason   = "$WorkingFileName was NOT copied to $NewFileName\n";
          $ErrorCode     = " $!";
          $EndProgram    = false;
          $ThereWasAnError = true;
          HandleErrors();
     };
    
  };
 
#############################**********************************##################################### 
#  renames the  current PageCounter.html file to be YYYY-MM-DD_PageCounter.html and 
#    moves it to the Archive Directory as specified in the ini file
#  rename the current ama.txt and resource.txt files to be YYYY-MM-DD_XXXX  and 
#    moves them to the archive directory as specified in the ini file
#
#

 sub MoveFile()
 {
  logIt("*******------  Entering MoveFile module  ------*******\n");    
    my ($WorkingFileName,$NewFileName) = @_; 
    if (! -e $WorkingFileName) {
          $ErrorLocation = "Sub MoveFile"; 
          $ErrorReason   = "$WorkingFileName can not be found";
          $ErrorCode     = " $!";
          $EndProgram    = false;
          $ThereWasAnError = true;
          HandleErrors();
    }
    
    if (! move ( $WorkingFileName, $NewFileName)) 
    {

          $ErrorLocation = "Sub MoveFile"; 
          $ErrorReason   = "$WorkingFileName was NOT moved to $NewFileName\n";
          $ErrorCode     = " $!";
          $EndProgram    = false;
          $ThereWasAnError = true;          
          HandleErrors();
  
          logIt("==> $WorkingFileName was NOT MOVED to $NewFileName\n");  
     };
    
  }; 
 
######################################################################################################
#############################******** File  Write  Logic ********##################################### 
###############################********************************####################################### 

#  WriteHTMLContents
# 
#   
sub WriteHTMLContents() {
    print OUTHTMLMenu $_[0] ;    
    ####print  $_[0] ;    
} 
 
######################################################################################################
#############################********  XLSX  File  Logic ********##################################### 
###############################********************************####################################### 
#  Load XLSX File  
## worksheet_1 title  = ActiveFiles
## worksheet 2 title  = ResourceFiles
## worksheet 3 title  = PageTotals
## write_col( $row, $column, $array_ref, $format )
sub LoadXLSXFile () {
     $workSheet1->write_col('A1',\@xlsWkSht1);
     $workSheet2->write_col('A1',\@xlsWkSht2);
     $workSheet3->write_col('A1',\@xlsWkSht3);
} 
 
#############################**********************************#####################################

sub OpenHtmlMenuIn {
     
     if (! open(INHTMLMenu,"<$outHTMLMenuDir$outHTMLMenu")) {
          $ErrorLocation      = "Sub OpenHtmlMenuIn";
          $ErrorReason        = "Unable to Open: $outHTMLMenuDir$outHTMLMenu";
          $ErrorCode          = "$!";
          $EndProgram         = false;
          $ThereWasAnError    = true;          
          HandleErrors();
          close(INHTMLMenu);
       }
       else
       {
          $| = 1; ## this causes the output to be written to immediately
          logIt("** Open HTMLMenu < Input: $outHTMLMenuDir$outHTMLMenu \n");          
        };  
     
}


#############################**********************************#####################################

sub OpenLogFileHtmlIn {

     if (! open(INHTMLLOGFILE,"<$htmlReportNameDir$htmlTemplateSelectorName")) {
          $ErrorLocation      = "Sub OpenHtmlIn";
          $ErrorReason        = "Unable to Open: $htmlReportNameDir$htmlTemplateSelectorName";
          $ErrorCode          = "$!";
          $EndProgram         = false;
          $ThereWasAnError    = true;          
          HandleErrors();
          close(INHTMLLOGFILE);
       }
       else
       {
          $| = 1; ## this causes the output to be written to immediately
          logIt("** Open  TemplatePageSelector < Input: $htmlReportNameDir$htmlTemplateSelectorName *\n");            
        };  
     
}

#############################**********************************#####################################

sub OpenHtmlLogFileOut() {
     
         if (! open(OutHTMLLOGFILE,">$outHTMLMenuDir$htmlSelectorName")) {
              $ErrorLocation      = "Sub OpenHtmlOut";
              $ErrorReason        = "Unable to Open: $outHTMLmenuDir$htmlSelectorName";
              $ErrorCode          = "$!";
              $EndProgram         = false;
              $ThereWasAnError    = true;          
              HandleErrors();
              close(OutHTMLLOGFILE);
           }
           else
           {
              $| = 1; ## this causes the output to be written to immediately
              logIt("** Open  HTMLLogFile >Output: $outHTMLMenuDir$htmlSelectorName *\n");            
            }
  }

#############################**********************************#####################################

sub OpenArchiveFileHtmlIn {

     if (! open(INHTMLARCHIVEFILE,"<$htmlReportNameDir$htmlTemplateArchivePageSelector")) {
          $ErrorLocation      = "Sub OpenHtmlIn";
          $ErrorReason        = "Unable to Open: $htmlReportNameDir$htmlTemplateArchivePageSelector";
          $ErrorCode          = "$!";
          $EndProgram         = false;
          $ThereWasAnError    = true;          
          HandleErrors();
          close(INHTMLARCHIVEFILE);
       }
       else
       {
          $| = 1; ## this causes the output to be written to immediately
          logIt("** Open  TemplateArchivePageSelector < Input: $htmlReportNameDir$htmlTemplateArchivePageSelector *\n");            
        };  
     
}

#############################**********************************#####################################

sub OpenHtmlArchiveFileOut() {
     
         if (! open(OutHTMLARCHIVEFILE,">$outHTMLMenuDir$htmlArchivePageSelector")) {
              $ErrorLocation      = "Sub OpenHtmlOut";
              $ErrorReason        = "Unable to Open: $outHTMLmenuDir$htmlArchivePageSelector";
              $ErrorCode          = "$!";
              $EndProgram         = false;
              $ThereWasAnError    = true;          
              HandleErrors();
              close(OutHTMLARCHIVEFILE);
           }
           else
           {
              $| = 1; ## this causes the output to be written to immediately
              logIt("** Open  HTMLArchiveFile >Output: $outHTMLMenuDir$htmlArchivePageSelector *\n");            
            }
  }

#############################**********************************#####################################

sub OpenHtmlIn {

     if (! open(INHTMLReport,"<$htmlReportNameDir$htmlOriginalReportName")) {
          $ErrorLocation      = "Sub OpenHtmlIn";
          $ErrorReason        = "Unable to Open: $htmlReportNameDir$htmlOriginalReportName";
          $ErrorCode          = "$!";
          $EndProgram         = false;
          $ThereWasAnError    = true;          
          HandleErrors();
          close(INHTMLReport);
       }
       else
       {
          $| = 1; ## this causes the output to be written to immediately
          logIt("** Open  OriginalHTMLReport < Input: $htmlReportNameDir$htmlOriginalReportName *\n");            
        };  
     
}

#############################**********************************#####################################

sub OpenHtmlOut() {
      my ($passFileType) = @_;
      
      if ($passFileType eq "ama") {
      
         if (! open(OutHTMLReport,">$outHTMLMenuDir$htmlAMAReportName")) {
              $ErrorLocation      = "Sub OpenHtmlOut";
              $ErrorReason        = "Unable to Open: $outHTMLmenuDir$htmlAMAReportName";
              $ErrorCode          = "$!";
              $EndProgram         = false;
              $ThereWasAnError    = true;          
              HandleErrors();
              close(OutHTMLReport);
           }
           else
           {
              $| = 1; ## this causes the output to be written to immediately
              logIt("** Open  HTMLReport >Output: $outHTMLMenuDir$htmlAMAReportName *\n");            
            };
      } else
      {
       
         if (! open(OutHTMLReport,">$outHTMLMenuDir$htmlResourceReportName")) {
              $ErrorLocation      = "Sub OpenHtmlOut";
              $ErrorReason        = "Unable to Open: $outHTMLMenuDir$htmlResourceReportName";
              $ErrorCode          = "$!";
              $EndProgram         = false;
              $ThereWasAnError    = true;          
              HandleErrors();
              close(OutHTMLReport);
           }
           else
           {
              $| = 1; ## this causes the output to be written to immediately
              logIt("** Open  HTMLReport >Output: $outHTMLMenuDir$htmlResourceReportName *\n");            
            };        
        
      }
     
}

#############################**********************************#####################################
 
 sub OpenHTMLMenuOut() {
      if (! open(OUTHTMLMenu,">$outHTMLMenuDir$outHTMLMenu")) {
          $ErrorLocation      = "Sub OpenOutputFiles";
          $ErrorReason        = "Unable to Open: $outHTMLMenuDir$outHTMLMenu";
          $ErrorCode          = "$!";
          $EndProgram         = false;
          $ThereWasAnError    = true;          
          HandleErrors();       
          close(OUTHTMLMenu);
       }
       else
       {
        $| = 1; ## this causes the output to be written to immediately
          logIt("** Open HTMLMenu > Output: $outHTMLMenuDir$outHTMLMenu *\n");        
        };
      }
 
#############################**********************************#####################################
sub OpenExcelFile() {
   logIt("*******------  Entering OpenExcelFile module  ------*******\n");    
    my $tmpName = $xlOutputDirectory.$xlFileName.".".$xlExtension;
  #print $tmpName;

## this next section is NOT implemented and require adding a new
##  module if it must be implemented - to create an xls instead of xlsx
   #####if (uc($xlExtension) eq ("XLS")) {
   #####  $workbook = Excel::Writer::XLS->new($tmpName); <==  need new module here
   #####} else {
   #####  $workbook = Excel::Writer::XLSX->new($tmpName);
   #####}
   
   $workbook = Excel::Writer::XLSX->new($tmpName);     
 
   if ($workbook eq undef ) { 
          $ErrorLocation      = "Sub OpenExcelFile";
          $ErrorReason        = "EXCEL file error: $tmpName Please close Excel!! ";
          $ErrorCode          = "$!";
          $EndProgram         = true;
          $ThereWasAnError    = true;          
          HandleErrors();       
   }

   $workSheet1 = $workbook->add_worksheet($worksheet_1);  ##  Active Files

   $workSheet2 = $workbook->add_worksheet($worksheet_2);  ## Resource Files

   $workSheet3 = $workbook->add_worksheet($worksheet_3);  #  Page Totals
   logIt("** Open the $xlExtension Output File and worksheets *\n");     
   $wkSht1RowCnt   = 0;   
   $wkSht2RowCnt   = 0;     
   $wkSht3RowCnt   = 0;
 
   my $format = $workbook->add_format( bold => 1);

    $workSheet1->set_column( 'A:A', 11.43 );
    $workSheet1->set_column( 'B:B', 18.14 );
    $workSheet1->set_column( 'C:C', 13.7 );    
    $workSheet1->set_column( 'D:D', 113.7 );
    $workSheet1->set_column( 'E:E',10);    
    
    $workSheet1->set_row( 0, 20, $format );    

    $workSheet2->set_column( 'A:A', 20 );
    $workSheet2->set_column( 'B:B', 36.45 );
    $workSheet2->set_column( 'C:C', 13.7 );    
    $workSheet2->set_column( 'D:D', 108);
    $workSheet2->set_column( 'E:E',10);
    
    $workSheet2->set_row( 0, 20, $format );    

    $workSheet3->set_column( 'A:A', 13.7 );
    $workSheet3->set_column( 'B:B', 14.3 );
    $workSheet3->set_column( 'C:C', 113.7 );
    $workSheet3->set_column( 'D:D', 10);    

    $workSheet3->set_row( 0, 20, $format );      
   
   $workSheet1->freeze_panes('F2');
   $workSheet2->freeze_panes('F2');
   $workSheet3->freeze_panes('E2');

   
   my $tmpFileDate = substr($FileDatePrefix,0,length($FileDatePrefix)-1);
   
   $xlsWkSht1[$wkSht1RowCnt][0]  = $wkSht1Col1;
   $xlsWkSht1[$wkSht1RowCnt][1]  = $wkSht1Col2;        
   $xlsWkSht1[$wkSht1RowCnt][2]  = $wkSht1Col3;         
   $xlsWkSht1[$wkSht1RowCnt][3]  = $wkSht1Col4;
   $xlsWkSht1[$wkSht1RowCnt][4]  = $tmpFileDate;
   
   $xlsWkSht2[$wkSht2RowCnt][0]  = $wkSht2Col1;
   $xlsWkSht2[$wkSht2RowCnt][1]  = $wkSht2Col2;
   $xlsWkSht2[$wkSht2RowCnt][2]  = $wkSht2Col3;         
   $xlsWkSht2[$wkSht2RowCnt][3]  = $wkSht2Col4;
   $xlsWkSht2[$wkSht2RowCnt][4]  = $tmpFileDate;
    
   $xlsWkSht3[$wkSht3RowCnt][0]  = $wkSht3Col1;
   $xlsWkSht3[$wkSht3RowCnt][1]  = $wkSht3Col2;
   $xlsWkSht3[$wkSht3RowCnt][2]  = $wkSht3Col3;
   $xlsWkSht3[$wkSht3RowCnt][3]  = $tmpFileDate;   
         
   $wkSht1RowCnt   = 2;   
   $wkSht2RowCnt   = 2;     
   $wkSht3RowCnt   = 2;         
}
#############################**********************************#####################################
#  PadIt takes in a date value,
#  one for day and then later for
#  month.  it looks at the length
#  of the value and if is only one
#  digit wide then it will prepend 
#  it with a 0 thus keeping it 
#  2 digits wide.  Also, we need to pad
#  some values to be 6 digits so now the
#  2nd parameter = 1 means pad 1 else
#  pad to 6 digits wide.

sub PadIt()
{
   if ($_[1] eq 1) {
    if (length($_[0]) == 1)   
    {
      $_[0] = ('0'. $_[0]);
     }
    }
    else
    {
     while (length($_[0]) < $_[1])
     {
       $_[0]  = ('0'. $_[0]);
     }
     };
 };
 #############################**********************************#####################################
# opens specified file for output...if it does not exist then it will be created
#    if it exists then it will be appendedTo.
#  input parameters: directory;  filename
sub OpenInputFileRead() {
   my ($dir, $file) = @_;
   
      if (! open(INFILEREAD,"<$dir$file")) {
          $ErrorLocation      = "Sub OpenInputFile";
          $ErrorReason        = "Unable to Open: $dir$file";
          $ErrorCode          = "$!";
          $EndProgram         = false;
          $ThereWasAnError    = true;          
          HandleErrors();
          close(INFILEREAD);
       }
       else
       {
        $| = 1; ## this causes the output to be written to immediately
          logIt("** Open the $dir$file Output *\n");
        };
 };


#############################**********************************#####################################
sub OpenLogFile() {
      if (! open(OUTFILE,">>$LogFileDirectory$LogFileName")) {
           print("** ERROR - Can't open $LogFileDirectory$LogFileName $!- ExitValue = 40 -\n");
          close(OUTFILE);
       }
       else
       {
        $| = 1;
        };
 };
 
#############################**********************************##################################### 
#  prints output to both the monitor output and the local log file
#  appends a date-time stamp to the passed in message which is contained
#  in the @_  perl variable.
 sub logIt
{
  print scalar(localtime(time)), ": ", @_;
  print OUTFILE scalar(localtime(time)), ": ", @_;
};

#############################**********************************##################################### 
sub  HandleErrors() {
      logIt("==> There are exceptions...please see exception log\n");

      logException("Error Location: $ErrorLocation \n");
      logException("Error Reason: $ErrorReason\n");
      logException("Error Code: $ErrorCode\n");
      
      if ($errorEmail eq "true") {
         $FromError = "true";
         $errorEmailMessage = "$errorEmailMessage \nError Location: $ErrorLocation  \nError Reason: $ErrorReason \nError Code: $ErrorCode \n";
         SendEmailOut();
      }

      if ($EndProgram) {
          close(OUTFILE);
          close(ExceptionOUTFILE);
          die;       
      }
   };

#############################**********************************#####################################
sub OpenExceptionFile() {
      if (! open(ExceptionOUTFILE,">>$LogFileDirectory$ExceptionFileName")) {
           print("** ERROR - Can't open $LogFileDirectory$ExceptionFileName $!- ExitValue = 40 -\n");           
          close(ExceptionOUTFILE);
       }
       else
       {
        $| = 1;
        ## redirects STDERR to the exception log
        ## this will be turned off at end of program  => look at where all file closes are performed
        STDERR->fdopen( \*ExceptionOUTFILE,  'w' ) or die $!;
        };
 };        

#############################**********************************##################################### 
#  prints output to both the monitor output and the local log file
#  appends a date-time stamp to the passed in message which is contained
#  in the @_  perl variable.
 sub logException
{   
  print scalar(localtime(time)), ": ", @_;
  print ExceptionOUTFILE scalar(localtime(time)), ": ", @_;
   
};
 
#############################**********************************##################################### 
#  sub SendEmailOut

sub SendEmailOut
   {
     logIt("*******------  Entering SendEmailOut module  ------*******\n");       


   my $tmpTo  ;
   my $tmpSubject;
   my $tmpMessage;
   my $tmpLogIt ;
  
   
      if ($FromError eq "true") {
          $tmpTo      = $errorRecipient;
          $tmpSubject = $errorEmailSubject;
          $tmpMessage = $errorEmailMessage;
          $tmpLogIt   = "ERROR Email sent Successfully\n";
         
      }else
      {
          $tmpTo      = $recipient;
          $tmpSubject = $emailSubject;
          $tmpMessage = $emailMessage;         
          $tmpLogIt   = "Email Sent Successfully\n"; 
      }
     
####Using Send::Mail
my %mail;


  %mail = (
   
    To          => $tmpTo,
    From        => 'dmckay@tbscg.com',
    Subject     => $tmpSubject,
    Body        => $tmpMessage

  );
  
  #sendmail(%mail) or die $Mail::Sendmail::error;   
  eval {sendmail(%mail)}; warn $@ if $@;
   if ($@) {
          $ErrorLocation      = "Sub SendEmail";
          $ErrorReason        = "Error while attempting to send and Email";
          $ErrorCode          = "$!";
          $EndProgram         = false;
          $ThereWasAnError    = true;          
          HandleErrors();
   }
  
  logIt $tmpLogIt;
      
   };
 
  
#############################***********  End of Program  ************#####################################  