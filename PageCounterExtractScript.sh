#!/bin/sh
printf 'creating Resource file\n'
find /opt/Interwoven/LSCSRT-Store/ -xdev -print0 | xargs -0 ls -dils --time-style=long-iso |grep "/resources/doc/" |awk '{if ($10 ~ /\.HTM/ || $10 ~ /\.pdf/ || $10 ~ /\.[a-zA-Z0-9]*/) print $10" "$8" "$9; else print $10"/ "$8" "$9}'  |awk  -F "/resources/doc/" '{print "www.ama-assn.org/resources/doc/"$2}' > ./resource/resource.txt;
printf 'creating PageCount  file\n'
find '/opt/Interwoven/WASAPPS/AMACellt1240/LiveSite Run Time.ear/iw-runtime.war/sites/' -xdev -print0 | xargs -0 ls -dils --time-style=long-iso |awk '{if ($12 ~ /\.page/ || $12 ~ /\.site/ || $12 == "Time.ear/iw-runtime.war/sites/") print $10" "$11" "$12" "$8" "$9; else print $10" "$11" "$12"/ "$8" "$9}' |awk -F "/sites" '{print "www.ama-assn.org"$2}' |sort > ./ama/ama.txt;
printf 'completed Creating all files\n'